*//
*//		REGULA EL VOLUMEN DE UNA LISTA-ARBOL DE MP3, BY ODRF++, PROBADO EN VFP9
*//
CLEAR
CLOSE TABLES ALL
CLEAR ALL
SET DATE TO ITALIAN
SET SAFETY OFF
SET TALK OFF
loSMP3=CREATEOBJECT( 'SCANMP3' )
loSMP3.INITSCAN()
RELEASE loSMP3
RETURN


*//
*//		CLASS
*//
DEFINE CLASS SCANMP3 AS CUSTOM
	* integer
	lnDROOT		=0
	lnSUBDIR	=0
	* arrays
	DIMENSION	laDROOT[1],laSUBDIR[1]
	* strings
	lcCURDIR	=SYS(5)+CURDIR()
	lcDIRSRC	='H:\TEMP\PENDRIVE\'
	lcDIRDST	='H:\TEMP\PENDRIV2\'
	lcEXEBIN	='H:\UTIL\FFMPEG\BIN\FFMPEG.EXE'
	lcFILEXT	='MP3'
	*///////////////////////////////////
	PROCEDURE INIT AS Boolean
	*/////////////////////////////////////
		*//
		*//	TODO:	INICIALIZACION
		*//
		RETURN .T.
	ENDPROC
	*/////////////////////////////////////////////////////////
	FUNCTION INITSCAN()
	*/////////////////////////////////////////////////////////
		*//
		*//	TODO:	INICIA EL ESCANEO DE UN DIRECTORIO & SUB
		*//
		LOCAL lnGIR
		
		* capturamos directorios de raiz
		THIS.lnDROOT=ADIR( THIS.laDROOT, THIS.lcDIRSRC+'*.*', 'D', 1 )
		IF THIS.lnDROOT==0
			? "FALLO EN ADIR 0x0",
			RETURN .T.
		ENDIF
		THIS.PUSHDIR(THIS.lcDIRSRC)
		FOR lnGIR=1 TO THIS.lnDROOT STEP 1
			
			DO CASE
				CASE THIS.laDROOT[lnGIR,1]=='.' OR THIS.laDROOT[lnGIR,1]=='..'
				CASE 'A'$THIS.laDROOT[lnGIR,5] AND UPPER(JUSTEXT(THIS.laDROOT[lnGIR,1]))==THIS.lcFILEXT
					THIS.SCANFILE(THIS.laDROOT[lnGIR,1])
				CASE 'D'$THIS.laDROOT[lnGIR,5]
					THIS.SCANDIR(THIS.laDROOT[lnGIR,1])
				OTHERWISE
			ENDCASE
			
			* debug
			*? "root-gir:",lnGIR,THIS.laDROOT[lnGIR,1],THIS.laDROOT[lnGIR,5]
			*INKEY(.01)
			*IF LASTKEY()==27
			*	EXIT
			*ENDIF
			
		ENDFOR
		THIS.POPDIR()
		
		RETURN .T.
	ENDFUNC
	*/////////////////////////////////////////////////////////
	FUNCTION SCANDIR(lcSUB AS String) AS Boolean
	*/////////////////////////////////////////////////////////
		*//
		*//	TODO:	ESCANEA UN DIRECTORIO EN BUSCA DE ARCHIVOS
		*//
		LOCAL lnGIR,laSUB[1],lnSUB,lcDIR
		
		* capturamos directorios de raiz
		THIS.PUSHDIR(lcSUB)
		lcDIR	=THIS.GETSUBDIR(1)
		*? "DIR",lcDIR,"SUB",lcSUB
		lnSUB	=ADIR( laSUB, lcDIR+'*.*', 'D', 1 )
		IF lnSUB==0
			? "FALLO EN ADIR 0x1",lcDIR+'*.*'
			RETURN .T.
		ENDIF
		FOR lnGIR=1 TO lnSUB STEP 1
			DO CASE
				CASE laSUB[lnGIR,1]=='.' OR laSUB[lnGIR,1]=='..'
				CASE 'A'$laSUB[lnGIR,5] AND UPPER(JUSTEXT(laSUB[lnGIR,1]))==THIS.lcFILEXT
					THIS.SCANFILE(laSUB[lnGIR,1])
				CASE 'D'$laSUB[lnGIR,5]
					THIS.SCANDIR(laSUB[lnGIR,1])
				OTHERWISE
			ENDCASE
			
			* debug
			*INKEY(.01)
			*IF LASTKEY()==27
			*	EXIT
			*ENDIF
			
		ENDFOR
		THIS.POPDIR()
		RETURN .T.
	ENDFUNC
	*//////////////////////////////////////////////////////////	
	FUNCTION SCANFILE(lcFILE AS STRING) AS Boolean
	*//////////////////////////////////////////////////////////
		*//
		*//	TODO:	PROCESA UN ARCHIVO	ffmpeg -i input.mp3 -af volume=1.5 output.mp3
		*//
		LOCAL lcSOURCE,lcDESTIN
		
		lcSOURCE=THIS.GETSUBDIR(1)+lcFILE
		lcDESTIN=STRTRAN(lcSOURCE,THIS.lcDIRSRC,THIS.lcDIRDST)
		lcDIRDST=LEFT(lcDESTIN,RAT('\',lcDESTIN,1))
		IF !DIRECTORY('"'+lcDIRDST+'"')
			MKDIR ('"'+lcDIRDST+'"')
		ENDIF
		lcCMD=THIS.lcEXEBIN+' -i "'+lcSOURCE+'" -af volume=1 "'+lcDESTIN+'"'
		? lcCMD
		THIS.LAUNCHEXE(lcCMD)
		RETURN .T.
	ENDFUNC
	*///////////////////////////////////////////////////////
	FUNCTION PUSHDIR(lcSUBDIR AS String) AS integer
	*///////////////////////////////////////////////////////
		*//
		*//	TODO:	PUSHDIR
		*//
		THIS.lnSUBDIR=THIS.lnSUBDIR+1
		DIMENSION THIS.laSUBDIR[THIS.lnSUBDIR]
		THIS.laSUBDIR[THIS.lnSUBDIR]=lcSUBDIR
		RETURN THIS.lnSUBDIR
	ENDFUNC
	*/////////////////////////////////////////////////////////
	FUNCTION POPDIR() AS Boolean
	*/////////////////////////////////////////////////////////
		*//
		*//	TODO:	POPDIR
		*//
		ADEL(THIS.laSUBDIR,THIS.lnSUBDIR)
		THIS.lnSUBDIR=THIS.lnSUBDIR-1
		RETURN .T.
	ENDFUNC
	*///////////////////////////////////////////////////////
	FUNCTION GETSUBDIR(lnFROM AS INTEGER) AS STRING
	*///////////////////////////////////////////////////////
		*//
		*//	TODO:	DEVUELVE CADENA COMPLETA DE SUBDIR
		*//
		LOCAL lcOUT,lnGIR
		lcOUT=''
		FOR lnGIR=lnFROM TO THIS.lnSUBDIR STEP 1
			lcOUT=lcOUT+THIS.laSUBDIR[lnGIR]+IIF(lnGIR==1,'','\')
		ENDFOR
		RETURN lcOUT
	ENDFUNC
	*///////////////////////////////////////////////////////
	FUNCTION LAUNCHEXE( lcCOMMAND AS String ) AS Boolean
	*///////////////////////////////////////////////////////
		*//
		*//	TODO:	LANZA UN EJECUTABLE Y ESPERA A QUE TERMINE
		*//
		LOCAL lcSTART,lcPROCESS_INFO,lnPROCESS
		IF TYPE( 'lcCOMMAND' )#'C'
			MESSAGEBOX( [EXE-RUN, PARAM1], 16, [ERROR] ) 
			RETURN .F.
		ENDIF
		DECLARE INTEGER CreateProcess IN kernel32.DLL STRING,STRING,INTEGER,INTEGER,INTEGER,INTEGER,INTEGER,INTEGER,STRING @,STRING @
		DECLARE INTEGER WaitForSingleObject IN kernel32.DLL INTEGER hHandle, INTEGER dwMilliseconds			&&
		lcSTART			=CHR(0)+CHR(0)+CHR(0)+CHR(68)+REPLICATE(CHR(0),64)									&&68bytes string
		lcPROCESS_INFO	=REPLICATE(CHR(0),16)																&&16bytes string
		lcCOMMAND		=STRCONV( lcCOMMAND+CHR(0), 1 )														&&add NULL CHAR, CONV TO WIDECHAR
		IF CreateProcess( NULL, lcCOMMAND, 0, 0, 1, 32, 0, 0, @lcSTART, @lcPROCESS_INFO )==0
			CLEAR DLLS "CreateProcess","WaitForSingleObject"
			MESSAGEBOX( [FAIL:]+lcCOMMAND, 16, [ERROR] )
			RETURN .F.
		ENDIF
		lnPROCESS=	(ASC(SUBSTR(lcPROCESS_INFO,1,1)))+;
					(ASC(SUBSTR(lcPROCESS_INFO,2,1))*256)+;
					(ASC(SUBSTR(lcPROCESS_INFO,3,1))*65536)+;
					(ASC(SUBSTR(lcPROCESS_INFO,4,1))*16777216)
		DO WHILE .T.
			IF WaitForSingleObject(lnPROCESS,200)#0x00000102
				EXIT
			ELSE
				DOEVENTS
			ENDIF
		ENDDO
		CLEAR DLLS "CreateProcess","WaitForSingleObject"
		RETURN .T.
	ENDFUNC
	*///////////////////////////////////////
	PROCEDURE RELEASE 
	*///////////////////////////////////////
		*//
		*//	TODO:	ELIMINA OBJETO
		*//
		CLOSE TABLES ALL
		CD(THIS.lcCURDIR)
	ENDPROC
ENDDEFINE